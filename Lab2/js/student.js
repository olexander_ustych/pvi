export class student {

    id;

    group;
    firstName;
    lastName;
    gender;
    birthday;

    get _birthday()
    {
        /*let correctBD = ''; 
        correctBD += this._birthday.substring(0, 4);
        correctBD += '-';
        correctBD += this._birthday.substring(5, 7);
        correctBD += '-';
        correctBD += this._birthday.substring(8, 10);

        return correctBD;*/
        return this.birthday;
    }

    set _birthday(value)
    {
        let correctBD = ''; 
        correctBD += value.substring(8, 10);
        correctBD += '.';
        correctBD += value.substring(5, 7);
        correctBD += '.';
        correctBD += value.substring(0, 4);

        this.birthday = correctBD;
    }

    constructor(id, group, firstName, lastName, gender, birthday) {

        this.id = id;

        this.group = group;
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;

        /*let correctBD = ''; 
        correctBD += birthday.substring(8, 10);
        correctBD += '.';
        correctBD += birthday.substring(5, 7);
        correctBD += '.';
        correctBD += birthday.substring(0, 4);

        this.birthday = correctBD;*/
        this._birthday = birthday;
    }

    getDate()
    {
        let correctBD = ''; 
        correctBD += this._birthday.substring(6, 10);
        correctBD += '-';
        correctBD += this._birthday.substring(3, 5);
        correctBD += '-';
        correctBD += this._birthday.substring(0, 2);

        return correctBD;
    }
}