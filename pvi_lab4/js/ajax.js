export async function saveOnServer(jsonStudent) {    
    //$('document').ready(async function() {
        //event.preventDefault();

        return new Promise((resolve, reject) => {
            $.ajax({
                type: 'POST',
                url: '../php/Controller/studentController.php',
                data: jsonStudent,
                success: function(response) {
                    let resp = JSON.parse(response);
    
                    if(Array.isArray(resp) == false)
                    {
                        console.log(resp); 
                        resolve(JSON.stringify({
                            status: true,
                            message: resp
                        }));   
                        return;
                    }
                    for(let someArr of resp)
                    {
                        console.log(someArr.errorMessage);
                    }
                    resolve(JSON.stringify({
                        status: false,
                        message: resp
                    }));
                },
                error(xhr,status,error) {
                    console.log("Fail of server! ", error);
                    reject(JSON.stringify({
                        status: false,
                        message: error
                    }));
                }
            });
        });
    
    //});  
}

export async function DelteFromServer(jsonStudent) {    
    return new Promise((resolve, reject) => {
        $.ajax({
            type: 'POST',
            url: '../php/Controller/studentController.php',
            data: jsonStudent,
            success: function(response) {
                let resp = JSON.parse(response);

                /*if(Array.isArray(resp) == false)
                {
                    console.log(resp); 
                    if (typeof variable === "object" && variable !== null && !Array.isArray(variable)) {
                        reject(resp);
                    } else {
                        resolve(JSON.stringify({
                            status: true,
                            message: resp
                        }));   
                        return;
                    }
                }*/

                console.log(resp);
                resolve(resp);
            },
            error(xhr,status,error) {
                console.log("Fail of server! ", error);
                reject(JSON.stringify({
                    status: false,
                    message: error
                }));
            }
        });
    });    
}

export async function synchronize(data) {    
    return new Promise((resolve, reject) => {
        $.ajax({
            type: 'POST',
            url: '../php/Controller/studentController.php',
            data: data,
            success: function(response) {
                let resp = JSON.parse(response);
                console.log(resp);
                //синхронізовано
                resolve(resp);
            },
            error(xhr,status,error) {
                console.log("Fail of server! ", error);
                reject(JSON.stringify({
                    status: false,
                    message: error
                }));
            }
        });
    });    
}

export async function edit(data) {    
    return new Promise((resolve, reject) => {
        $.ajax({
            type: 'POST',
            url: '../php/Controller/studentController.php',
            data: data,
            success: function(response) {
                let resp = JSON.parse(response);
                console.log("Changed: " + resp);
                //синхронізовано
                resolve(resp);
            },
            error(xhr,status,error) {
                console.log("Fail of server! ", error);
                reject(JSON.stringify({
                    status: false,
                    message: error
                }));
            }
        });
    });    
}

export function serverCreateFile() {    
    $('document').ready(function() {    
        $.ajax({
            type: 'GET',
            url: '../php/index.php',
            success: function(response) {
                console.log(response);
            },
            error(xhr,status,error) {
                console.log("Fail of server! ", error);
                return false;
            }
        });
    }); 
}

export function checkUser(jsonData) {

    return new Promise((resolve, reject) => {
        $.ajax({
            type: 'POST',
            url: '../php/Controller/userController.php',
            data: jsonData,
            success: function(response) {
                let answer = JSON.parse(response);
                if(answer == true)
                {
                    resolve(true);
                }
                else
                {
                    resolve(false);
                }
            }, 
            error(error) {
                console.log("Server fail!", error);
                reject(false);
            }
        });
    });
}

export function signUp(jsonData) {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: 'POST',
            url: '../php/Controller/userController.php',
            data: jsonData,
            success: function(response) {
                let answer = JSON.parse(response);
                if(answer == true)
                {
                    resolve(true);
                }
                else
                {
                    resolve(false);
                }
            }, 
            error(error) {
                console.log("Server fail!", error);
                reject(false);
            }
        });
    });
}

export function getUserForm()
{
    return new Promise((resolve, reject) => {
        $.ajax({
            type: 'GET',
            url: '../php/Controller/userController.php',
            data: {
                action: "getForm",
                Login: null,
                Password: null
            },
            success: function(response) {
                let answer = JSON.parse(response);

                let position = document.querySelector('.div-form');
                position.innerHTML = answer;

                resolve(answer);
            }, 
            error(error) {
                console.log("Server fail!", error);
                reject(false);
            }
        });
    });
}

export function getStudentsFunc()
{
    return new Promise((resolve, reject) => {
        $.ajax({
            type: 'GET',
            url: '../php/Controller/studentController.php',
            data: {
                action: "getForm",
            },
            success: function(response) {
                let answer = JSON.parse(response);               
                resolve(answer);                
                reject(answer);
            }, 
            error(error) {
                console.log("Server fail!", error);
                reject(false);
            }
        });
    });   
}

export function getStudentsTable()
{
    return new Promise((resolve, reject) => {
        $.ajax({
            type: 'GET',
            url: '../php/Controller/studentController.php',
            data: {
                action: "getStudentsTable",
            },
            success: function(response) {
                let answer = JSON.parse(response);                              
                resolve(answer);
                reject(answer);
            }, 
            error(error) {
                console.log("Server fail!", error);
                reject(false);
            }
        });
    });   
}