import { checkUser, serverCreateFile, getUserForm } from "./ajax.js";

import { allFunc } from './add/userScript.js';

window.addEventListener('load', async () => {
    if('serviceWorker' in navigator)
    {
        try{
            let reg = await navigator.serviceWorker.register('../html/sw.js');
            console.log("Service worker register successfully", reg);
        } catch(err){
            console.log(err);
            console.log("Service worker register fail");
        }
    }

     await getUserForm().then(
        function(result){ allFunc(); }
     );
    //serverCreateFile();
});

let a_span_point = document.querySelector('.notification');
let span_point = document.querySelector('.point');

a_span_point.addEventListener('dblclick', () => {
    span_point.style.display = 'block';
});

a_span_point.addEventListener('mouseover', () => {
    let newDiv = document.querySelector('.newWindow');
    newDiv.style.display = 'flex';

    /*let dialog = document.querySelector('.dialog')
    dialog.show();*/
});

a_span_point.addEventListener('mouseout', () => {
    let newDiv = document.querySelector('.newWindow');
    newDiv.style.display = 'none';
});

let profile = document.querySelectorAll('.equalize-imgs'); 
for(let i = 0; i < profile.length; i++)
{
    let newDiv = document.querySelector('.profile');
    newDiv.style.display = 'none';

    if(i == 0) {
        continue;
    }

    profile[i].addEventListener('click', () => {
        if(newDiv.style.display == 'block')        {
            newDiv.style.display = 'none';
        }else if (newDiv.style.display == 'none') {
            newDiv.style.display = 'block';
        }
    });
}
