<?php

require_once('../Model/user.php');
require_once('../View/userView.php');

if($_SERVER['REQUEST_METHOD'] === 'GET')
{
    if(isset($_GET['action']))
    {
        $action = $_GET['action'];
        switch($action) {
            case 'getForm':
                $controller = new userController(null);
                $controller->getForm();
                break;
            default:
                print(json_encode("Eroor action!"));
                break;
        }
    }   
    else
    {
        print(json_encode("Fail action: " . $_GET['action']));
    }
}
if($_SERVER['REQUEST_METHOD'] === 'POST')
{
    $data = json_decode(file_get_contents("php://input"), true);       
    if($data['action'])
    {
        $action = $data['action'];
        switch ($action) {
            case 'check':                
                $user = new user($data['Login'], $data['Password']);
                $controller = new userController($user);
                $controller->check();
                break;
            case 'signUp':
                $user = new user($data['Login'], $data['Password']);
                $controller = new userController($user);
                $controller->signUp();
                break;
            default:
                print(json_encode("Error action!"));
                break;
        }
    }
    else
    {
        print(json_encode("Fail action: " . $_POST['action']));
    }
}


class userController
{
    public $user;
    private $serv_conn;

    function __construct($user)
    {
        $this->user = $user;

        try {
            $servername = "localhost";
            $username = "root";
            $password = "";
            $conn = new PDO("mysql:host=$servername;dbname=my_db", 
                $username, $password);
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            $this->serv_conn = $conn;

            //print(json_encode("Connected successfully")); 
        }
        catch(PDOException $e)
        {
            print(json_encode("Connection failed: " . $e->getMessage()));
        }       
    }

    function getForm()
    {
        $view = new userView($this->user, $this);
        $html = $view->getView();
        print(json_encode($html));
    }

    function check()
    {
        try {
            // Prepare and execute a SELECT statement
            $users = $this->serv_conn->query("SELECT * FROM users");
            $rows = $users->fetchAll(PDO::FETCH_ASSOC);          

            foreach ($rows as $row)
            {
                if ($row["Login"] == $this->user->Login && 
                    $row["Password"] == $this->user->Password)
                {
                    print(json_encode(true));
                    return;
                }
            }   
            print(json_encode(false));
        } catch (PDOException $e) {
            print(json_encode("Connection failed: " . $e->getMessage()));
        }        
    }

    function signUp()
    {
        try {
            // Prepare and execute a SELECT statement
            $l = $this->user->Login;
            $p = $this->user->Password;

            $users = $this->serv_conn->query("SELECT * FROM users");
            $rows = $users->fetchAll(PDO::FETCH_ASSOC);          

            foreach ($rows as $row)
            {
                if ($row["Login"] == $l && 
                    $row["Password"] == $p)
                {
                    print(json_encode(false));    
                    return;                
                }
            }       
            $this->serv_conn->query("INSERT INTO users
                VALUES ('$l', $p)");            

            print(json_encode(true));
            return;
            
            
        } catch (PDOException $e) {
            print(json_encode("Connection failed: " . $e->getMessage()));
        }        
    }
}
?>