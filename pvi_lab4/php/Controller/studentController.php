<?php

use studentController as GlobalStudentController;

require_once("../Model/student.php");
require_once("../View/studentView.php");

if($_SERVER['REQUEST_METHOD'] === 'GET')
{
    if(isset($_GET['action']))
    {
        $action = $_GET['action'];
        switch($action) {
            case 'getForm':
                $controller = new studentController(null);
                $controller->getForm();
                break;
            case 'getStudentsTable':
                $controller = new studentController(null);
                $controller->getStudentsTable();
                break;
            default:
                print(json_encode("Eroor action!"));
                break;
        }
    }   
    else
    {
        print(json_encode("Fail action: " . $_GET['action']));
    }
}
if($_SERVER['REQUEST_METHOD'] === 'POST')
{
    $data = json_decode(file_get_contents("php://input"), true);       
    if($data['action'])
    {
        $action = $data['action'];
        $error = array();
        switch ($action) {
            case 'add':
                $student = new student($data['id'], 
                    $data['group'], $data['firstName'], 
                    $data['lastName'], $data['gender'],
                    $data['birthday']);
                checking($student, 'save', $error);
                if(count($error) > 0)
                {
                    print(json_encode($error));
                    return;
                }
                $controller = new studentController($student);
                $controller->add();
                break;
            case 'edit':
                $student = new student($data['id'], 
                    $data['group'], $data['firstName'], 
                    $data['lastName'], $data['gender'],
                    $data['birthday']);
                checking($student, 'save', $error);
                if(count($error) > 0)
                {
                    print(json_encode($error));
                    return;
                }
                $controller = new studentController($student);
                $controller->edit();
                break;
            case 'delete':
                $student = new student($data['id'], '', 
                    '', '', '', '');
                $controller = new studentController($student);
                $controller->delete();
                break;
            case 'synchronize':
                $controller = new studentController(null);
                $controller->synchronize($data);
                break;
            default:
                print(json_encode("Error action!"));
                break;
        }
    }
    else
    {
        print(json_encode("Fail action: " . $data['action']));
    }
}

class studentController
{
    public $student;
    private $serv_conn;

    function __construct($student)
    {
        $this->student = $student;

        try {
            $servername = "localhost";
            $username = "root";
            $password = "";
            $conn = new PDO("mysql:host=$servername;dbname=my_db", 
                $username, $password);
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            $this->serv_conn = $conn;

            //print(json_encode("Connected successfully")); 
        }
        catch(PDOException $e)
        {
            print(json_encode("Connection failed: " . $e->getMessage()));
        }       
    }

    function getForm()
    {
        $view = new studentView($this->student, $this);
        $html = $view->getView();
        print(json_encode($html));
    }

    function getStudentsTable()
    {
        try {
            $users = $this->serv_conn->query("SELECT * FROM students");
            $rows = $users->fetchAll(PDO::FETCH_ASSOC);         
            
            $arrStudents = array();
            foreach ($rows as $row)
            {
                array_push($arrStudents, new student($row['id'], 
                $row['groupField'], $row['firstName'], $row['lastName'],
                $row['gender'], $row['birthday']));
            }   
            print(json_encode($arrStudents));
        } catch (PDOException $e) {
            print(json_encode("Connection failed: " . $e->getMessage()));
        }
    }

    function add()
    {
        try {          
            $id = $this->student->id;
            //print(json_encode($id));
            $gr = $this->student->group;
            //print(json_encode($gr));
            $f = $this->student->firstName;
            //print(json_encode($f));
            $l = $this->student->lastName;
            //print(json_encode($l));
            $ge = $this->student->gender;
            //print(json_encode($ge));
            $b = new DateTime($this->student->birthday);
            $b = $b->format("Y-m-d");
            //print(json_encode($b));          

            $sql = "INSERT INTO students 
            (id, groupField, firstName, lastName, gender, birthday) 
            VALUES ($id, '$gr', '$f', '$l', '$ge', '$b')";
            $this->serv_conn->query($sql);

            $res = array(
                "status" => true,
                "message" => "The student saved successfully!"
            );

            print(json_encode($res));
        } catch (PDOException $e) {
            print(json_encode("Connection failed: " . $e->getMessage()));
        }
    }

    function delete()
    {
        try {
            //check is exist

            $sql = "SELECT COUNT(*) FROM students WHERE id = :id";
            $prepare = $this->serv_conn->prepare($sql);
            $prepare->bindParam(':id', $this->student->id);
            $prepare->execute();
            $result = $prepare->fetchColumn();

            if($result <= 0)
            {
                $res = array(
                    "status" => false,
                    "message" => "This object doesn't exist!"
                );
                print(json_encode($res));   
                return;
            }

            //delete
            $idOfSt = $this->student->id;
            $sql = "DELETE FROM students WHERE 
                id = ($idOfSt)";
            $this->serv_conn->query($sql);

            $res = array(
                "status" => true,
                "message" => "The student deleted successfully!"
            );

            print(json_encode($res));
        } catch (PDOException $e) {
            print(json_encode("Connection failed: " . $e->getMessage()));
        }
    }

    function synchronize($data)
    {
        try{
            //print(json_encode($data));
            foreach ($data['arr'] as $key => $value) {                

                $sql = "SELECT COUNT(*) FROM students WHERE id = :id";
                $prepare = $this->serv_conn->prepare($sql);
                $oldId = $value['old'];
                $prepare->bindParam(':id', $oldId);
                $prepare->execute();
                $result = $prepare->fetchColumn();

                if($result == 0)
                {
                    continue;
                }                

                $newId = $value['new'];
                //print(json_encode($newId));

                $name = $value['student']['firstName'];
                $lastName = $value['student']['lastName'];
                $gr = $value['student']['group'];
                $gender = $value['student']['gender'];
                $b = $value['student']['birthday'];
                
                /*$sql = "UPDATE students 
                SET id = $newId 
                WHERE (groupField = '$gr' AND 
                firstName = '$name' AND
                lastName = '$lastName' AND
                gender = '$gender' AND
                birthday = '$b')";*/
                //print(json_encode($sql));

                /*$sql1 = "UPDATE students 
                SET id = :new_id 
                WHERE firstName = :first_name
                AND lastName = :last_name
                AND groupField = :group_field
                AND gender = :gender
                AND birthday = :birthday";*/

                $sql1 = "UPDATE students 
                SET id = :new_id 
                WHERE id = :old_id";

                $prepare1 = $this->serv_conn->prepare($sql1);
                $prepare1->bindParam(':new_id', $newId);
                $prepare1->bindParam(':old_id', $oldId);
                /*$prepare1->bindParam(':first_name', $name);
                $prepare1->bindParam(':last_name', $lastName);
                $prepare1->bindParam(':group_field', $gr);
                $prepare1->bindParam(':gender', $gender);
                $prepare1->bindParam(':birthday', $b);*/
                $prepare1->execute();

            }            
            print(json_encode("Database synchronized successfully!"));
        } catch (PDOException $e) {
            print(json_encode("Connection failed: " . $e->getMessage()));
        }
    }

    function edit()
    {
        try {
            //check is exist
            $sql = "SELECT COUNT(*) FROM students WHERE id = :id";
            $prepare = $this->serv_conn->prepare($sql);
            $prepare->bindParam(':id', $this->student->id);
            $prepare->execute();
            $result = $prepare->fetchColumn();

            if($result <= 0)
            {
                $res = array(
                    "status" => true,
                    "message" => "The student doesn't edited!"
                );;
                print(json_encode($res));   
                return;
            }

            $id = $this->student->id;
            $gr = $this->student->group;
            $f = $this->student->firstName;
            $l = $this->student->lastName;            
            $ge = $this->student->gender;            
            $b = new DateTime($this->student->birthday);
            $b = $b->format("Y-m-d");
            

            //edit
            $idOfSt = $this->student->id;
            $sql = "UPDATE students 
            SET groupField = '$gr',
            firstName = '$f',
            lastName = '$l',
            gender = '$ge',
            birthday = '$b'
            WHERE id = ($id)";
            $this->serv_conn->query($sql);

            $res = array(
                "status" => true,
                "message" => "The student edited successfully!"
            );;

            print(json_encode($res));
        } catch (PDOException $e) {
            print(json_encode("Connection failed: " . $e->getMessage()));
        }
    }
}

function checking($student, $saveORedit, &$error)
{
    if($student->group == 0)
    {
        $error[] = array('status' => false,
         'errorMessage' => "Failed to $saveORedit student in the table, the group can't be empty!");
    }
    if (($student->firstName == "") || (preg_match('/^[A-Za-z]+$/', $student->firstName) == false))
    {
        $error[] = array('status' => false,
        'errorMessage' => "Failed to $saveORedit student in the table, the firstName can't be empty and can contain only letters!");
    }
    if (($student->lastName == "") || (preg_match('/^[A-Za-z]+$/', $student->lastName) == false))
    {
        $error[] = array('status' => false,
        'errorMessage' => "Failed to $saveORedit student in the table, the lastName can't be empty or can contain only letters!");
    }
    if ($student->gender == 0)
    {
        $error[] = array('status' => false,
        'errorMessage' => "Failed to $saveORedit student in the table, the gender can't be empty!");
    }
    if (strlen($student->birthday) < 5)
    {
        $error[] = array('status' => false,
        'errorMessage' => "Failed to $saveORedit student in the table, the birthdat can't be empty!");
    }
    else
    {
        $today = new DateTime(date('Y-m-d'));
        $date = new DateTime($student->birthday);
        
        if( ($today->diff($date)->y < 16) || ($today->diff($date)->y > 70))
        {
            $error[] = array('status' => false,
            'errorMessage' => "Failed to $saveORedit student in the table, the birthdat aren't correct!");
        }
    }
    
}

?>