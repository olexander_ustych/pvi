<?php
  
class studentView
{
    private $model;
    private $controller;
    private $html1;
    private $html2;
    private $html;

    public function __construct($_model, $_controller)
    {
        $this->model = $_model;
        $this->controller = $_controller;
        $this->html1 = file_get_contents('../additional/createNedit.html');
        $this->html2 = file_get_contents('../additional/warning.html');
        $this->html = array($this->html1, $this->html2);
    }

    public function getView()
    {
        return $this->html;
    }
}

?>