const ST_CACHE_NAME = "st-pwa-cashe";

const allStFileUrls = [
    'index.html',
    '../js/index.js',
    '../js/student.js',
    '../css/index.css', 
    '../img/Знімок екрана 2023-03-05 140728.png',
    '../img/notif.jpg'
];

self.addEventListener('install', async event => {
    console.log('install');
    /*event.waitUntil(
        caches.open(ST_CASHE_NAME)
        .then(cache => cache.addAll(allStFileUrls))
    );*/
    const cache = await caches.open(ST_CACHE_NAME);
    await cache.addAll(allStFileUrls);
});

self.addEventListener('activate', async event => {
    console.log('activate');

    const cacheNames = await caches.keys();
    await Promise.all(
        cacheNames.filter(name => name !== ST_CACHE_NAME)
        .map(name => caches.delete(name))
    );
});

self.addEventListener('fetch', event => {
    console.log('Fetch: ', event.request.url);

    event.respondWith(cacheFirst(event.request));
});

async function cacheFirst(request) {
    const cached = await caches.match(request);
    console.log(cached === null);       // при true праює fetch
    return cached ?? await fetch(request);
}