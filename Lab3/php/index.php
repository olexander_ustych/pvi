<?php
function checking($data, $saveORedit, &$error)
{
    if($data['group'] == 0)
    {
        $error[] = array('status' => false,
         'errorMessage' => "Failed to $saveORedit student in the table, the group can't be empty!");
    }
    if (($data['firstName'] == "") || (preg_match('/^[A-Za-z]+$/', $data['firstName']) == false))
    {
        $error[] = array('status' => false,
        'errorMessage' => "Failed to $saveORedit student in the table, the firstName can't be empty and can contain only letters!");
    }
    if (($data['lastName'] == "") || (preg_match('/^[A-Za-z]+$/', $data['lastName']) == false))
    {
        $error[] = array('status' => false,
        'errorMessage' => "Failed to $saveORedit student in the table, the lastName can't be empty or can contain only letters!");
    }
    if ($data['gender'] == 0)
    {
        $error[] = array('status' => false,
        'errorMessage' => "Failed to $saveORedit student in the table, the gender can't be empty!");
    }
    if (strlen($data['birthday']) < 3)
    {
        $error[] = array('status' => false,
        'errorMessage' => "Failed to $saveORedit student in the table, the birthdat can't be empty!");
    }
    $today = new DateTime(date('Y-m-d'));
    $date = new DateTime($data['birthday']);
    if( ($today->diff($date)->y < 16) || ($today->diff($date)->y > 70))
    {
        $error[] = array('status' => false,
        'errorMessage' => "Failed to $saveORedit student in the table, the birthdat aren't correct!");
    }
}


require_once 'student.php';

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $array_of_student = array();
    
    if(file_exists('db.txt'))
    {
        $file = fopen("db.txt", 'r');
        while(!feof($file))
        {
            $line = fgets($file);
            if(strlen($line) > 1)
            {
                $data_from_file = json_decode($line);
                $array_of_student[] = new student($data_from_file->id,
                    $data_from_file->group, $data_from_file->firstName, 
                    $data_from_file->lastName, $data_from_file->gender,
                    $data_from_file->birthday);
            }
        }
        fclose($file);
    }
    //print_r($array_of_student);

    $data = json_decode(file_get_contents("php://input"), true); 
    $error = array();
    $saveORedit = "";

    $save = true;
    for($i = 0; $i < count($array_of_student); $i++)
    {
        if($data['id'] == $array_of_student[$i]->id)
        {
            $edit = 'edite';
            $saveORedit = $edit;
            checking($data, $edit, $error);
            $array_of_student[$i] = new student($data['id'], $data['group'], $data['firstName'],
                $data['lastName'], $data['gender'], $data['birthday']);
            $save = false;
            break;
        }
    }

    if($save == true)
    {
        $save = 'save';
        $saveORedit = $save;
        checking($data, $save, $error);
        $array_of_student[] = new student($data['id'], $data['group'], $data['firstName'],
            $data['lastName'], $data['gender'], $data['birthday']);
    }
    //print_r($array_of_student);

    $file = fopen("db.txt", 'w');
    for($i = 0; $i < count($array_of_student); $i++)
    {
        $str = json_encode($array_of_student[$i]);
        $str = $str."\n";
        fprintf($file, $str);
    }
    fclose($file);

    if(count($error) != 0)
    {
        print(json_encode($error));
    }
    else
    {
        print(json_encode("The student is $saveORedit"."d successfully!"));
    }
}

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    $file = fopen("db.txt", 'w');
    fclose($file);
    print("File created!");
}
?>