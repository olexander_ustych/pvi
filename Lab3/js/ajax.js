export async function saveOnServer(jsonStudent) {    
    //$('document').ready(async function() {
        //event.preventDefault();

        return new Promise((resolve, reject) => {
            $.ajax({
                type: 'POST',
                url: '../php/index.php',
                data: jsonStudent,
                success: function(response) {
                    let resp = JSON.parse(response);
    
                    if(Array.isArray(resp) == false)
                    {
                        console.log(resp); 
                        resolve(JSON.stringify({
                            status: true,
                            message: resp
                        }));   
                    }
                    for(let someArr of resp)
                    {
                        console.log(someArr.errorMessage);
                    }
                    resolve(JSON.stringify({
                        status: false,
                        message: resp
                    }));
                },
                error(xhr,status,error) {
                    console.log("Fail of server! ", error);
                    reject(JSON.stringify({
                        status: false,
                        message: error
                    }));
                }
            });
        });
    
    //});  
}

export function serverCreateFile() {    
    $('document').ready(function() {    
        $.ajax({
            type: 'GET',
            url: '../php/index.php',
            success: function(response) {
                console.log(response);
            },
            error(xhr,status,error) {
                console.log("Fail of server! ", error);
                return false;
            }
        });
    }); 
}

export function checkUser(jsonData) {

    return new Promise((resolve, reject) => {
        $.ajax({
            type: 'POST',
            url: '../php/form.php',
            data: jsonData,
            success: function(response) {
                let answer = JSON.parse(response);
                if(answer == true)
                {
                    resolve(true);
                }
                else
                {
                    resolve(false);
                }
            }, 
            error(error) {
                console.log("Server fail!", error);
                reject(false);
            }
        });
    });
}