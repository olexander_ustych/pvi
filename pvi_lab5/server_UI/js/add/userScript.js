import { checkUser, getStudentsTable, getStudentsFunc, 
    saveOnServer, signUp } from "../ajax.js";
//import { checkUser, serverCreateFile, getUserForm } from "../ajax.js";
import { student } from "../student.js";
import { loadTableScript, arrStud, createJson } from './tableScript.js';

export let logined = false;

export function checkLogined()
{
    if(Qs.parse(location.search, {
        ignoreQueryPrefix: true
    }).user ==  document.querySelectorAll('.name')[1].innerHTML)
    {
        logined = true;
    }
}

export function allFunc()
{
    document.getElementById('btnForm').addEventListener('click', async () => {
        let obj = {
            Login: document.getElementById('nameForm').value,
            Password: document.getElementById('passwordForm').value,
            action: 'check'
        };
    
        let jsonObj = JSON.stringify(obj);
        let answer = await checkUser(jsonObj);
        if(answer == false)
        {
            alert("Такого користувача не існує!");
            document.getElementById('nameForm').value = "";
            document.getElementById('passwordForm').value = "";
        }
        else
        {
            document.querySelector('.div-form').style.display = "none";
            document.querySelector('main').style.display = "block";

            loginIsTrue(obj);

            await getStudentsFunc().then(
                function(resolve){
                    let main = document.querySelector("main");
                    let dialogs = "";
                    resolve.forEach(element => {
                        dialogs += element;
                    });
                    let after = main.innerHTML;
                    main.innerHTML = dialogs;
                    main.innerHTML += after;
                }
            );

            await getStudentsTable().then(
                function(resolve) {
                    resolve.forEach(element => {
                        let newStudent = new student(element.id, 
                            element.group, 
                            element.firstName,
                            element.lastName, 
                            element.gender, 
                            element.birthday);
                        let table = document.querySelector('main > div > table > tbody');
    
                        table.insertAdjacentHTML('beforeend', `
                            <tr>
                                <td><input type="checkbox"></td>
                                <td><b>${newStudent.group}</b></td>
                                <td><b>${newStudent.firstName + " " + newStudent.lastName}</b></td>
                                <td><b>${newStudent.gender}</b></td>
                                <td><b>${newStudent.birthday}</b></td>
                                <td><button class="status"></button></td>
                                <td><input type="button" value="&#9997;" class="edit"><input type="button" value="&#215" class="delete"></td>
                            </tr>
                        `);

                        arrStud.push(newStudent);
                        addBtnsFunctional(newStudent, arrStud.length - 1);
                    });
                },
                function(reject) {
                    alert("База даних пуста!");
                }
            ).then(
                function(resolve) {
                    loadTableScript();
                }
            );
        }
    });


    document.getElementById('btnSignUp').addEventListener('click', async () =>{
        let obj = {
            Login: document.getElementById('nameForm').value,
            Password: document.getElementById('passwordForm').value,
            action: 'signUp'
        };
    
        let jsonObj = JSON.stringify(obj);
        let answer = await signUp(jsonObj);
        if(answer == false)
        {
            alert("Даний користувач вже зареєстровний!");
            document.getElementById('nameForm').value = "";
            document.getElementById('passwordForm').value = "";
        }
        else
        {
            document.querySelector('.div-form').style.display = "none";
            document.querySelector('main').style.display = "block";

            loginIsTrue(obj);

            await getStudentsFunc().then(
                function(resolve){
                    let main = document.querySelector("main");
                    let dialogs = "";
                    resolve.forEach(element => {
                        dialogs += element;
                    });
                    let after = main.innerHTML;
                    main.innerHTML = dialogs;
                    main.innerHTML += after;
                }
            );

            await getStudentsTable().then(
                function(resolve) {
                    resolve.forEach(element => {
                        let newStudent = new student(element.id, 
                            element.group, 
                            element.firstName,
                            element.lastName, 
                            element.gender, 
                            element.birthday);
                        let table = document.querySelector('main > div > table > tbody');
    
                        table.insertAdjacentHTML('beforeend', `
                            <tr>
                                <td><input type="checkbox"></td>
                                <td><b>${newStudent.group}</b></td>
                                <td><b>${newStudent.firstName + " " + newStudent.lastName}</b></td>
                                <td><b>${newStudent.gender}</b></td>
                                <td><b>${newStudent.birthday}</b></td>
                                <td><button class="status"></button></td>
                                <td><input type="button" value="&#9997;" class="edit"><input type="button" value="&#215" class="delete"></td>
                            </tr>
                        `);

                        arrStud.push(newStudent);
                        addBtnsFunctional(newStudent, arrStud.length - 1);
                    });
                },
                function(reject) {
                    alert("База даних пуста!");
                }
            ).then(
                function(resolve) {
                    loadTableScript();
                }
            );
        }
    });
    
}

function loginIsTrue(obj)
{
    logined = true;
    document.querySelectorAll('.name')[1].innerHTML = obj.Login;
}



function addBtnsFunctional(newStudent, indexOfStudent) {
    let btnToDelete = document.querySelectorAll('.delete');
    btnToDelete[indexOfStudent].student = newStudent; 
    for(let btn of btnToDelete){
        btn.addEventListener('click', () => {
            let dialogWarm = document.querySelector('.warning');
            dialogWarm.btnDelete = btn;
            dialogWarm.show();
    
            //btnToDelete.parentNode.parentNode.remove();
        });
    }


    let btnOutDialog = document.querySelector('dialog > div > div > button');
    btnOutDialog.addEventListener('click', outOfDialog);

    let btnCancel = document.getElementById('cancel');
    btnCancel.addEventListener("click", outOfDialog);

    function outOfDialog()
    {
        let group = document.getElementById('Group');
        let inputs = document.querySelectorAll('.input-dialog');
        let gender = document.getElementById('Gender');
        //занулюємо поля діалогу
        group.value = 0;
        for(let inp of inputs)
        {
            inp.value = '';
        }
        gender.value = 0;
        //
        dialog.close();
    }
    let btnOk = document.querySelector('.btn-in-footer-diag > div > button');


    //editing
    let btnToEdit = document.querySelectorAll('.edit');
    btnToEdit[newStudent.id].refOnStudent = newStudent;
    btnToEdit[newStudent.id].addEventListener('click', () => {

        let dialog = document.querySelector('dialog');
        let h = dialog.childNodes[1].childNodes[1].firstElementChild;
        h.hidden = true;

        let newH = document.createElement('h3');
        newH.innerText = "Edit student";
        h.after(newH);

        // нова кнопка Cancel
        let newCancel = document.createElement('button');
        newCancel.innerText = "Cancel";
        btnCancel.hidden = true;
        btnCancel.after(newCancel);
        newCancel.addEventListener('click', exitOfEditDiag);

        //хрестик хверху
        let newBtnOutDialog = document.createElement('button');
        newBtnOutDialog.innerText = document.querySelector('dialog > div > div > button').innerText;
        for(let some of btnOutDialog.style)
        {
            newBtnOutDialog.style[some] = btnOutDialog.style.getPropertyValue(some);
        }     
        btnOutDialog.hidden = true;
        btnOutDialog.after(newBtnOutDialog);
        newBtnOutDialog.addEventListener('click', exitOfEditDiag);  

        function exitOfEditDiag() 
        {
            if(newH != null)
            {
                newH.remove();
                h.hidden = false;
            }

            if(newOK != null)
            {
                newOK.remove();
                btnOk.hidden = false;
            }

            if(newCancel != null)
            {
                newCancel.remove();
                btnCancel.hidden = false;
            }

            if(newBtnOutDialog != null)
            {
                newBtnOutDialog.remove();
                btnOutDialog.hidden = false;
            }

            dialog.close();

            //занулюємо поля діалогу
            let group = document.getElementById('Group');
            let inputs = document.querySelectorAll('.input-dialog');
            let gender = document.getElementById('Gender');

            group.value = 0;
            for(let inp of inputs)
            {
                inp.value = '';
            }
            gender.value = 0;
            //
        }

        // зміниити ok
        let newOK = document.createElement('button');
        newOK.innerText = 'OK';
        btnOk.hidden = true;
        btnOk.after(newOK);

        newOK.addEventListener('click', async () => {
            let group = document.getElementById('Group');
            let inputs = document.querySelectorAll('.input-dialog');
            let gender = document.getElementById('Gender');
            if(document.getElementById('checkingkBox').checked == false) {
                if(group.value == 0)
                {
                    showWarning(newOK, newCancel, "The group field can't be empty!");
                    return;
                }
                for (let i = 0; i < inputs.length - 1; i++)
                {
                    //const regex = new RegExp('1', '2', '3', '4', '5', '6', '7', '8', '9', '0');
                    const patternt = /^[a-zA-Z]+$/;   // /\d/; - тіки цифри //  /d+/;
                    if(patternt.test(inputs[i].value) == false)
                    {
                        let message = "";
                        if(i == 0)
                        {
                            message = "The name field can contain only letters!";
                        }
                        else
                        {
                            message = "The surname field can contain only letters!";
                        }

                        showWarning(newOK, newCancel, message);
                        return;
                    }
                }
                for (let i = 0; i < inputs.length; i++) 
                {
                    if(inputs[i].value == "")
                    {
                        let wnDiag = document.querySelector('.warning');
                        let message = "";
                        switch (i) {
                            case 0:
                                message = "The name field can't be empty!";
                                break;
                            case 1:
                                message = "The surname field can't be empty!";
                                break;
                            case 2:
                                message = "The date field can't be empty!";
                                break;    
                            default:
                                break;
                        }  

                        showWarning(newOK, newCancel, message);
                        return;
                    }    
                }
                if(gender.value == 0)
                {
                    showWarning(newOK, newCancel, "The gender field can't be empty!");
                    return;
                }
                let date = new Date(inputs[2].value);
                let today = new Date();
                if( (today.getFullYear() - date.getFullYear()) < 16 || (today.getFullYear() - date.getFullYear()) > 70)
                {
                    showWarning(btnOk, btnCancel, "The date isn't correct!");            
                    return;
                }
            }

            let EditedStudent = new student(btnToEdit[newStudent.id].refOnStudent.id, group.value, inputs[0].value, inputs[1].value, gender.value, inputs[2].value);   
            /*EditedStudent.action = 'edit';
            // на сервер
            if(await edit(JSON.stringify(EditedStudent)) == false)
            {
                showWarning(btnOk, btnCancel, "Changes are not applied!");
                return;
            }
            //*/
            let serverAnswer = await saveOnServer(createJson(EditedStudent, "edit"));
            let serverJson = await JSON.parse(serverAnswer);
            if(serverJson.status == false)
            {
                let message = "";
                for(let someArr of serverJson.message)
                {
                    message += someArr.errorMessage + "\n";
                }

                showWarning(btnOk, btnCancel, message);
                console.log("Server doesn't save changes!");
                return;
            } 
            btnToEdit[newStudent.id].refOnStudent = EditedStudent;

            let trToChange = btnToEdit[newStudent.id].parentNode.parentNode.cells;
            trToChange[1].innerHTML = `<b>${btnToEdit[newStudent.id].refOnStudent.group}</b>`;
            trToChange[2].innerHTML = `<b>${btnToEdit[newStudent.id].refOnStudent.firstName + " " + btnToEdit[newStudent.id].refOnStudent.lastName}</b>`;
            trToChange[3].innerHTML = `<b>${btnToEdit[newStudent.id].refOnStudent.gender}</b>`;
            trToChange[4].innerHTML = `<b>${btnToEdit[newStudent.id].refOnStudent.birthday}</b>`;                         

            exitOfEditDiag();            
        });                

        //вставити в діалог студента
        let sectionsToChange = document.querySelectorAll('.section');

        for(let i = 0; i < sectionsToChange[0].childNodes[3].length; i++)
        {
            if(sectionsToChange[0].childNodes[3][i].innerText == btnToEdit[newStudent.id].refOnStudent.group)
            {
                sectionsToChange[0].childNodes[3][i].selected = true;
                break;
            }
        }

        sectionsToChange[1].childNodes[3].value = btnToEdit[newStudent.id].refOnStudent.firstName;
        sectionsToChange[2].childNodes[3].value = btnToEdit[newStudent.id].refOnStudent.lastName;

        for(let i = 0; i < sectionsToChange[3].childNodes[3].length; i++)
        {
            if(sectionsToChange[3].childNodes[3][i].value == btnToEdit[newStudent.id].refOnStudent.gender)
            {
                sectionsToChange[3].childNodes[3][i].selected = true;
                break;
            }
        }

        sectionsToChange[4].childNodes[3].value = btnToEdit[newStudent.id].refOnStudent.getDate();
        //            
        
        dialog.show();        
    });

    let btnStatus = document.querySelectorAll('.status');
    for(let btn of btnStatus)
    {   
        btn.style.backgroundColor = 'green';
    }
    for(let btn of btnStatus)
    {
        btn.addEventListener('click', () => {
            if(btn.style.backgroundColor == 'green')
            {
                btn.style.backgroundColor = 'gray';
            }else if(btn.style.backgroundColor == 'gray')
            {
                btn.style.backgroundColor = 'green';
            }
        });
    }

    function showWarning(btnOkToDisable, btnCancelToDisable, message) 
    {
        btnOkToDisable.disabled = true;
        btnCancelToDisable.disabled = true;

        let wnDiag = document.querySelector('.warning');
        wnDiag.children[1].children[0].innerText = message;//"The group field can't be empty!";
        
        let btnWnOk = document.getElementById('wnOK');
        btnWnOk.hidden = true;

        let btnCancel2 = document.getElementById('cancel2');
        btnCancel2.hidden = true;

        let btnWnOutDialog = document.querySelector('.warning > div > div > button');
        btnWnOutDialog.hidden = true;

        let btnToAccept = document.createElement('button');
        btnToAccept.innerText = "OK";
        btnCancel2.after(btnToAccept);
        btnToAccept.addEventListener('click', () => {
            wnDiag.children[1].children[0].innerText = "Are u sure u want to delete user?";
            btnWnOk.hidden = false;
            btnCancel2.hidden = false;
            btnWnOutDialog.hidden = false;
            wnDiag.close();
            btnToAccept.remove();

            btnOkToDisable.disabled = false;
            btnCancelToDisable.disabled = false;
        });
        
        wnDiag.show();
    }
}