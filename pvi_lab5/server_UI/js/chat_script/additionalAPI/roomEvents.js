import { notify, socket, username } from "../chat.js"
export let uID;
export let room;
export let activeRoom;

export function personalRoom( myID, userID) {
    if(this.style.backgroundColor == 'lightblue')
    {
        return;
    }
    notify(false);

    uID = userID;
    if(myID < userID)
    {
        room = 'room' + myID + userID;
    }
    else
    {
        room = 'room' + userID + myID;
    }

    if(room != undefined)
    {
        socket.emit('personalRoom', room, uID);
        activeRoom = room;
    }
    else
    {
        console.log("Room is empty!");
    }

    let board = document.querySelector('.board-for-messages');
    for (let index = 0; index < board.children.length; index++) {
        board.children[index].remove();
        index--;        
    }

    const list = document.querySelector('.container-list-chat');
    for (let i = 0; i < list.children[1].children.length; i++) {
        if(list.children[1].children[i].style.backgroundColor != 'red')
        {
            list.children[1].children[i].style.backgroundColor = 'white';
        }
    }
    this.style.backgroundColor = 'lightblue';   

    const arrNames = [];
    arrNames.push(this.children[1].children[0].innerHTML);
    laodMembers(arrNames);

    document.querySelector('.mess-container > div > p').innerHTML = 
        `Chatroom with <b>${this.children[1].children[0].innerHTML}<b>`;
}

export function laodMembers(names) {
    let ul = document.querySelector('.mess-container > div > div > ul');
    for (let i = 0; i < ul.children.length; i++) {
        if(ul.children[i].children[0].classList.contains('plus-symbol'))
        {
            continue;
        }
        ul.children[i].remove();
    }


    names.forEach(element => {
        if(element != username)
        {
            let div = document.createElement('div');
            div.classList.add('member');
            div.innerHTML = 
            `<div class="chat-icon-text">
                <img src="../img/Знімок екрана 2023-03-05 140728.png" alt="" width="50" height="50">
            </div>
            <div class="chat-icon-text">
                <p class="chats-name">${element}</p>
            </div>`;
    
            let li = document.createElement('li');
            li.append(div);
    
            ul.append(li);
        }

    });
}

export function groupChat(chatName, members) {
    activeRoom = chatName;
    socket.emit('groupChat', chatName, members);
}

export function groupChatLoad(chatName, chatUpper)
{
    notify(false);

    const list = document.querySelector('.container-list-chat');
    for (let i = 0; i < list.children[1].children.length; i++) {
        if(list.children[1].children[i].style.backgroundColor != 'red')
        {
            list.children[1].children[i].style.backgroundColor = 'white';
        }
    }
    this.style.backgroundColor = 'lightblue'; 

    let board = document.querySelector('.board-for-messages');
    for (let index = 0; index < board.children.length; index++) {
        board.children[index].remove();
        index--;        
    }

    let ul = document.querySelector('.mess-container > div > div > ul');
    for (let i = 0; i < ul.children.length; i++) {
        ul.children[i].remove();
        i--;        
    }


    activeRoom = chatName;
    room = chatName;
    chatUpper.innerHTML = `<strong>Chatroom: ${chatName}<strong>`;
    this.style.backgroundColor = 'lightblue';
    socket.emit('loadGroupChatMembersNMsgs', chatName);
}