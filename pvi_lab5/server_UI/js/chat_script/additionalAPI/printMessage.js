export function printMessage(message) {
    let div = document.createElement('div');
    div.classList.add('message');
    div.innerHTML = 
        `<p class="name-time">${message.userName} <span>${message.time}</span></p>
        <p>${message.text}</p>`;
    document.querySelector('.board-for-messages').appendChild(div);
}