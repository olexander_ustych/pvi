// визначаємо висоту 
const list = document.querySelector('.container-list-chat');
const board = document.querySelector('.mess-container');

list.children[1].style.height = 
    (board.offsetHeight - list.children[0].offsetHeight - 
        getComputedStyle(list.children[0]).margin.split('').filter(
            letter => letter != 'p' && letter != 'x').join('') * 2
                ) + 'px';

document.querySelector('.field-to-write').focus();



import { printMessage } from "./additionalAPI/printMessage.js";
import { laodMembers, personalRoom , groupChat, activeRoom, groupChatLoad } from "./additionalAPI/roomEvents.js";

//користувач
const dataFromURL = Qs.parse(location.search, {
    ignoreQueryPrefix: true
});
export const username = dataFromURL.user;
console.log(username);
document.querySelectorAll('.name')[1].innerHTML = username;
document.querySelectorAll('aside > ul > li')[2].children[0]
.setAttribute('href', `http://localhost:3000/html/todo.html?user=${username}`);

// сокети юзаєм
export const socket = io();

// ідентифікуємо користувача
socket.emit('identifyUser', username);

let allUsers;
// завантажуємо користувачів
socket.on('users', users => {
    allUsers = users;

    let list_chat = document.querySelector('.list-chat');
    let myID;
    users.forEach(element => {
        if(element.Login == username)
        {
            myID = element.id;
        }
    });
    users.forEach(element => {
        if(element.Login != username)
        {            
            let div = document.createElement('div');
            div.classList.add('container-chats-fr');
            div.innerHTML = `
            <div class="chat-icon-text">
                <img src="../img/Знімок екрана 2023-03-05 140728.png" alt="" width="50" height="50">
            </div>
            <div class="chat-icon-text">
                <p class="chats-name">${element.Login}</p>
            </div>
            `;
            div.addEventListener('click', () => {
                let func = personalRoom.bind(div);
                func(myID, element.id);
            });
            list_chat.append(div);
        }
    });
});

socket.on('message', (message, roomID) => {
    if(activeRoom != roomID)
    {
        if(roomID.includes('room'))
        {
            let person;
            for(let i = 0; i < allUsers.length; i++)
            {
                if(roomID.includes((allUsers[i].id).toString()))
                {
                    if(allUsers[i].Login != username)
                    {
                        person = allUsers[i].Login;
                    }
                }
            }

            let list_chat = document.querySelector('.list-chat');
            for(let i = 0; i < list_chat.children.length; i++)
            {     
                if(list_chat.children[i].children[1].children[0].innerHTML == 
                    person)
                {
                    list_chat.children[i].style.backgroundColor = 'red';
                    notify(true);
                }
            }
        }
        else
        {
            let list_chat = document.querySelector('.list-chat');
            for(let i = 0; i < list_chat.children.length; i++)
            {
                if(list_chat.children[i].children[1].children[0].innerHTML == 
                    roomID)
                {
                    list_chat.children[i].style.backgroundColor = 'red';
                    notify(true);
                }
            }
        }
        return;
    }

    console.log(message);
    printMessage(message);

    //переміщення вниз, коли нове повідомлення
    document.querySelector('.board-for-messages').scrollTop =
        document.querySelector('.board-for-messages').scrollHeight;
});


import { uID, room } from "./additionalAPI/roomEvents.js";

const chatSendLine = document.querySelector('.btn-send');
chatSendLine.addEventListener('click', (e) => {
    e.preventDefault();

    /*if(uID == undefined)
    {
        alert("Оберіть чат!");
        return;
    }*/
    if(room == undefined)
    {
        alert("Проблеми з кімнатою");
        return;
    }

    const msg = document.querySelector('.field-to-write').value;
    document.querySelector('.field-to-write').value = "";
    //console.log(msg);

    // для всіх окрім того, хто відправив
    // то на стороні сервера так, 
    // тут це тіпа відправлення на сервер
    socket.emit('chatMessage', room, msg);
});

const arrOfMemvers = [];
document.querySelector('.header-chat').children[1].addEventListener('click',
() => {
    let chatRoomName = "";
    do {
        chatRoomName = prompt("Enter name of chatroom");
        if(chatRoomName == null)
        {
            return;
        }
        if(chatRoomName == "")
        {
            alert("Enter the name of chatroom!");
        }
    } while(chatRoomName == "")

    /*let datalist = document.getElementById('datalist-members');
    let list_chat = document.querySelector('.list-chat');

    for (let i = 0; i < list_chat.children.length; i++) {
        let name = list_chat.children[i].children[1].children[0].innerHTML;
        
        let opt = document.createElement('option');
        opt.setAttribute('value', name);
        datalist.append(opt);
    }

    let dialog = document.querySelector('.dialog');
    dialog.show();*/


    document.querySelector('.mess-container > div > p').innerHTML = 
        `Chatroom: <b>${chatRoomName}<b>`;

    let list_chat = document.querySelector('.list-chat');

    let div = document.createElement('div');
    div.classList.add('container-chats-fr');
    div.innerHTML = `
    <div class="chat-icon-text">
        <img src="../img/Знімок екрана 2023-03-05 140728.png" alt="" width="50" height="50">
    </div>
    <div class="chat-icon-text">
        <p class="chats-name">${chatRoomName}</p>
    </div>
    `;
    div.addEventListener('click', () => {
        let func = groupChatLoad.bind(div);
        func(chatRoomName,
            document.querySelector('.mess-container > div > p'));
    });
    div.addEventListener('dblclick', () => {
        do {
            chatRoomName = prompt("Enter name of chatroom", chatRoomName);
            if(chatRoomName == null)
            {
                return;
            }
            if(chatRoomName == "")
            {
                alert("Enter the name of chatroom!");
            }
        } while(chatRoomName == "")
    });

    const list = document.querySelector('.container-list-chat');
    for (let i = 0; i < list.children[1].children.length; i++) {
        if(list.children[1].children[i].style.backgroundColor != 'red')
        {
            list.children[1].children[i].style.backgroundColor = 'white';
        }
    }

    div.style.backgroundColor = 'lightblue';   
    list_chat.append(div);

    let board = document.querySelector('.board-for-messages');
    for (let index = 0; index < board.children.length; index++) {
        board.children[index].remove();
        index--;        
    }



    // додаємо плюсик
    let ul = document.querySelector('.mess-container > div > div > ul');
    for (let i = 0; i < ul.children.length; i++) {
        ul.children[i].remove();
    }
    let plus = document.createElement('div');
    plus.classList.add('plus-symbol');
    plus.innerHTML = 
    `<div class="horizontal-line"></div>
    <div class="vertical-line"></div>
    `;
    plus.addEventListener('click', () => {
        let dialog = document.querySelector('.dialog-chat');
        allUsers.forEach(element => {
            if(element.Login != username)
            {            
                let div = document.createElement('div');
                div.classList.add('container-chats-fr');
                div.innerHTML = `
                <div class="chat-icon-text">
                    <img src="../img/Знімок екрана 2023-03-05 140728.png" alt="" width="50" height="50">
                </div>
                <div class="chat-icon-text">
                    <p class="chats-name">${element.Login}</p>
                </div>
                `;                
                dialog.children[0].append(div);

                div.addEventListener('click', () => {
                    if(div.style.backgroundColor != 'lightgreen')
                    {
                        div.style.backgroundColor = 'lightgreen';
                        arrOfMemvers.push(div.children[1].children[0].innerHTML);
                    }
                    else
                    {
                        div.style.backgroundColor = 'white';
                        let item = div.children[1].children[0].innerHTML;
                        let index = arrOfMemvers.findIndex(element => element == item);
                        arrOfMemvers.splice(index, 1);
                        //alert(arrOfMemvers);
                    }
                });
            }
        });
        dialog.style.display = 'flex';
        dialog.show();

        let dialog_plus_cancel =
            document.querySelectorAll('.btn-in-footer-diag > div > button');
        dialog_plus_cancel[0].addEventListener('click', () => {
            laodMembers(arrOfMemvers);
            dialog.style.display = 'none';
            dialog.close();

            arrOfMemvers.push(username);
            let func = groupChat.bind(div);
            func(chatRoomName, arrOfMemvers);
        });
        dialog_plus_cancel[1].addEventListener('click', () => {
            dialog.style.display = 'none';
            dialog.close();
        });
    });

    let li = document.createElement('li');
    li.append(plus);

    ul.append(li);
});

/*document.getElementById('datalist-input').addEventListener('input', () => {
    let datalist = document.getElementsById('datalist-members');

    let nextDatalist = document.createElement('datalist');
    nextDatalist.setAttribute()
    for (let i = 0; i < list_chat.children.length; i++) {
        let name = list_chat.children[i].children[1].children[0].innerHTML;
     
        for(let data of datalist)
        {
            if(data.value == name)
            {
                continue;
            }
        }
        
        let opt = document.createElement('option');
        opt.setAttribute('value', name);
        datalist.append(opt);
    }
});*/


socket.on('addNewRoom', async (chatName) => {
    await socket.emit('addNewRoom', chatName);

    let list_chat = document.querySelector('.list-chat');
    /*list_chat.children.forEach(ch => {
        if(ch.children[1].children[0].innerHTML == chatName)
        {
            return;
        }
    });*/

    for(let blockName of list_chat.children)
    {
        if(blockName.children[1].children[0].innerHTML == chatName)
        {
            return;
        }
    }

    let div = document.createElement('div');
    div.classList.add('container-chats-fr');
    div.innerHTML = `
    <div class="chat-icon-text">
        <img src="../img/Знімок екрана 2023-03-05 140728.png" alt="" width="50" height="50">
    </div>
    <div class="chat-icon-text">
        <p class="chats-name">${chatName}</p>
    </div>
    `;
    div.addEventListener('click', () => {
        let func = groupChatLoad.bind(div);
        func(chatName,
            document.querySelector('.mess-container > div > p'));
    });
    list_chat.append(div);  
});

socket.on('loadMembers', (members, roomID) => {
    if(activeRoom == roomID)
    {
        laodMembers(members.members);
    }
    /*let ul = document.querySelector('.mess-container > div > div > ul');
    for (let i = 0; i < ul.children.length; i++) {
        if(members.include(
            ul.children[i].children[1].children[0].innerHTML))
        {
            continue;
        }  
    } */
});

socket.on("UnreadChats", unreadChats => {
    let person;
    if(unreadChats.includes('room'))
    {
        allUsers.forEach(e => {
            if(e.Login != username)
            {
                if(unreadChats.includes(e.id))
                {
                    person = e.Login;
                }
            }
        });

        let list_chat = document.querySelector('.list-chat');        
        for(let blockName of list_chat.children)
        {
            if(blockName.children[1].children[0].innerHTML == person)
            {
                blockName.style.backgroundColor = 'red';
            }
        }
    }
    else
    {
        let list_chat = document.querySelector('.list-chat');        
        for(let blockName of list_chat.children)
        {
            if(blockName.children[1].children[0].innerHTML == unreadChats)
            {
                blockName.style.backgroundColor = 'red';
            }
        }
    }
    notify(true)
});

document.getElementById('leave-btn').addEventListener('click', () => {
    let list_chat = document.querySelector('.list-chat');        

    for(let blockName of list_chat.children)
    {
        let dlt = true;
        if(blockName.style.backgroundColor == 'lightblue')
        {
            allUsers.forEach(element => {
                if(element.Login == blockName.children[1].children[0].innerHTML)
                {
                    alert("Неможливо!");
                    dlt = false;
                    return;
                }
            });

            if(dlt)
            {
                socket.emit('leaveRoom', blockName.children[1].children[0].innerHTML);
                blockName.remove();
            }
        }
    }
});

export function notify(yes) {
    let span_point = document.querySelector('.point');
    if(yes)
    {
        span_point.innerHTML++;
        span_point.style.display = 'block';
    }
    else
    {
        span_point.innerHTML--;
        if(span_point.innerHTML == 0)
        {
            span_point.style.display = 'none';
        }
    }
}