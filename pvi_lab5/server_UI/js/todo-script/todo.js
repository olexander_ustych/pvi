const dataFromURL = Qs.parse(location.search, {
    ignoreQueryPrefix: true
});
export const username = dataFromURL.user;
document.querySelectorAll('.name')[1].innerHTML = username;

const dialog = document.getElementById('1d');

document.querySelectorAll('.container')[0]
.children[0].addEventListener('click', () => {
    dialog.show();
});

loadAllTasks();

document.getElementById('add').addEventListener('click', () => {
    const text = dialog.children[1].children[1].value;
    const date = dialog.children[2].children[1].value;

    console.log(text);
    console.log(date);

    fetch('/todo', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json;charset=utf-8'
        },
        body: JSON.stringify({text: text, 
        date: date,
        user: username,
        action: 'todo'})
    }).then(res => {
        return res.json()
    }).then(res => {
        console.log(res);
    });    

    dialog.close();
    location.reload(true);
});

document.getElementById("cancelWithAdd").addEventListener('click', () => {
    dialog.close();
});



function loadAllTasks() {
    fetch(`/loadAllTasks?user=${username}`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json;charset=utf-8'
        }
    })
    .then(res => {
        return res.json();
    })
    .then(res => {
        let arrays = res.arrays;
        tasksUI(arrays);
    })
    .catch(err => {
        console.log("Error loading tasks!", err);
    });
}

let divUsing;
function tasksUI(tasks) {
    const containers = document.querySelectorAll('.container');
    for (let i = 0; i < containers.length; i++) {
        for (let j = 0; j < tasks[i].length; j++) {

            let action;
            switch (i) {
                case 0:
                    action = 'todo';
                    break;
                case 1:
                    action = 'inProcess';
                    break;
                case 2: 
                    action = 'done';
                    break;
                default:
                    break;
            }

            let div = document.createElement('div');
            div.classList.add('newTask');
            div.innerHTML = `<p>${tasks[i][j].text.substr(0, 10) + "..."}</p>
            <p>Date: ${tasks[i][j].date}</p>
            <p style="display: none;">${action}</p>
            <p style="display: none">${tasks[i][j]._id}</p>`;
    
            div.addEventListener('click', () => {
                divUsing = div;

                const dialogShowing = document.getElementById('2d');

                dialogShowing.children[1].children[1].value = tasks[i][j].text;
                dialogShowing.children[2].children[1].value = tasks[i][j].date;

                dialogShowing.show();

                document.getElementById('change').addEventListener('click', () => {
                    let action = div.children[2].innerHTML;
                    fetch('/todo/change', {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json;charset=utf-8'
                        },
                        body: JSON.stringify({text: dialogShowing.children[1].children[1].value, 
                        date: dialogShowing.children[2].children[1].value,
                        user: username,
                        action: action, 
                        _id: div.children[3].innerHTML
                    })
                    }).then(res => {
                        return res.json()
                    }).then(res => {
                        console.log(res);
                    });    
                    dialogShowing.close();

                    div.children[0].innerHTML = dialogShowing.children[1].children[1].value;
                    div.children[1].innerHTML = "Date: " + dialogShowing.children[2].children[1].value;

                    location.reload(true);
                });
                document.getElementById("close").addEventListener('click', () => {
                    dialogShowing.close();
                });                
            });

            containers[i].children[1].append(div); 
        }
    }
}

document.getElementById('ex').addEventListener('click', () => {
    let next = divUsing.children[2].innerHTML;
    const dialogShowing = document.getElementById('2d');

    let action;
    switch (next) {
        case 'todo':
            if(action == 'todo')
            {
                alert('Це неможливо');
            }
            return;
        case 'inProcess':
            action ='todo';
            break;
        case 'done':
            action = 'inProcess';
            break;
        default:
            break;        
    }

    fetch('/todo/changeTaskAction', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json;charset=utf-8'
        },
        body: JSON.stringify({
            text: dialogShowing.children[1].children[1].value, 
            date: dialogShowing.children[2].children[1].value,
            user: username, 
            _id: divUsing.children[3].innerHTML,
            ex: next, 
            next: action
        })
    })
    .then(res => {return res.json();})
    .then(res => {
        console.log(res);
    })
    .catch(err => {
        console.log("Fetch error", error);
    });

    dialogShowing.close();
    location.reload(true);

});
document.getElementById('next').addEventListener('click', () => {
    let ex = divUsing.children[2].innerHTML;
    const dialogShowing = document.getElementById('2d');

    let action;
    switch (ex) {
        case 'todo':
            action = 'inProcess';
            break;
        case 'inProcess':
            action ='done';
            break;
        case 'done':
            if(action == 'done')
            {
                alert('Це неможливо');
            }
            return;
        default:
            break;
    }

    fetch('/todo/changeTaskAction', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json;charset=utf-8'
        },
        body: JSON.stringify({
            text: dialogShowing.children[1].children[1].value, 
            date: dialogShowing.children[2].children[1].value,
            user: username, 
            _id: divUsing.children[3].innerHTML,
            ex: ex, 
            next: action
        })
    })
    .then(res => {return res.json();})
    .then(res => {
        console.log(res);
    })
    .catch(err => {
        console.log("Fetch error", error);
    });

    dialogShowing.close();
    location.reload(true);
});

