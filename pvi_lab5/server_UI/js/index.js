import { checkUser, serverCreateFile, getUserForm, userToNode } from "./ajax.js";

import { allFunc, logined, checkLogined } from './add/userScript.js';

window.addEventListener('load', async () => {
    if(window.location == 'http://localhost/pvi_lab5/server_UI/html/index.html')
    {
        if('serviceWorker' in navigator)
        {
            try{
                let reg = await navigator.serviceWorker.register('../html/sw.js');
                console.log("Service worker register successfully", reg);
            } catch(err){
                console.log(err);
                console.log("Service worker register fail");
            }
        }
    }

    if(window.location == 'http://localhost/pvi_lab5/server_UI/html/index.html')
    {        
        await getUserForm().then(
           function(result){ allFunc(); }
        );
        //serverCreateFile();
    }

});

let a_span_point = document.querySelector('.notification');
let span_point = document.querySelector('.point');

a_span_point.addEventListener('dblclick', async () => {
    //span_point.style.display = 'block';

    //checkLogined();
    let username;
    try {
        const dataFromURL = Qs.parse(location.search, {
            ignoreQueryPrefix: true
        });
        username = dataFromURL.user;
    }
    catch(err)
    {
        username = undefined;
    }

    if(logined == true || username != undefined)
    {
        //await userToNode();
        window.location.href = 
            `http://localhost:3000/html/chat.html?user=${document.querySelectorAll('.name')[1].innerHTML}`;
    }
    else
    {
        alert("Ввійдіть спочатку!");
    }
});

let isChatOpen = false;
a_span_point.addEventListener('click', () => {
    if(isChatOpen == false)
    {
        let newDiv = document.querySelector('.newWindow');
        newDiv.style.display = 'flex';
        isChatOpen = true;
    }
    else if(isChatOpen == true)
    {
        let newDiv = document.querySelector('.newWindow');
        newDiv.style.display = 'none';
        isChatOpen = false;
    }

    /*let dialog = document.querySelector('.dialog')
    dialog.show();*/
});

/*a_span_point.addEventListener('click', () => {
    
});*/

let profile = document.querySelectorAll('.equalize-imgs'); 
for(let i = 0; i < profile.length; i++)
{
    let newDiv = document.querySelector('.profile');
    newDiv.style.display = 'none';

    if(i == 0) {
        continue;
    }

    profile[i].addEventListener('click', () => {
        if(newDiv.style.display == 'block')        {
            newDiv.style.display = 'none';
        }else if (newDiv.style.display == 'none') {
            newDiv.style.display = 'block';
        }
    });
}

let aside = document.querySelector('aside > ul');
for (let i = 0; i < aside.children.length; i++) {
    let element = aside.children[i];
    switch (element.children[0].innerText) {
        case "Tasks":
            /*element.children[0].attributes['href'] = 
                `http://localhost:3000/html/todo.html?user=${document.querySelectorAll('.name')[1].innerHTML}`;*/
            
            if(document.querySelectorAll('.name')[1].innerHTML != "")
            {
                element.children[0]
                    .setAttribute('href', `http://localhost:3000/html/todo.html?user=${document.querySelectorAll('.name')[1].innerHTML}`);
            }

            break; 
        case "Students":
            /*element.children[0].attributes['href'] =
                `http://localhost/pvi_lab5/server_UI/html/index.html`;*/
            element.children[0]
                .setAttribute('href', `http://localhost/pvi_lab5/server_UI/html/index.html`);
            break;   
        default:
            break;
    }
}