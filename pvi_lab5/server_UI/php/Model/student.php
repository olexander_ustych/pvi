<?php

class student {
    public $id;
    
    public $group;
    public $firstName;
    public $lastName;
    public $gender;
    public $birthday;

    function __construct($id, $group, $firstName, $lastName, $gender, $birthday)
    {
        $this->id = $id;
        $this->group = $group;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->gender = $gender;
        $this->birthday = $birthday;
    }
}

?>