<?php

class userView
{
    private $model;
    private $controller;
    private $html;

    public function __construct($_model, $_controller)
    {
        $this->model = $_model;
        $this->controller = $_controller;
        $this->html = file_get_contents('../additional/logIn.html');
    }

    public function getView()
    {
        return $this->html;
    }
}

?>