const express = require('express');
const http = require('http');
const socketio = require('socket.io');

const app = express();
const server = http.createServer(app);
const io = socketio(server);

const messageAPI = require('./api/messages');

// підключаємося до бази даних
const dbAPI = require('./api/myDBapi');
dbAPI.connect();

// підключаємося до монго
dbAPI.connectToMongoDB();

// моє api для монго
const myMongoAPI = require('./api/myMongoDB_API');
//myMongoAPI.trying();

// вставновлюємо статичні файли
const path = require('path');
const { model } = require('mongoose');
const messages = require('./api/messages');
app.use(express.static(path.join(__dirname, '../server_UI')))

// всі користувачі
const allUsers = [];

// запускаємо коли клієет під'єднався
io.on('connection', (socket) => {
    //console.log("New websocket conn");

    let userName = '';
    let myUserID;
    // ідентифікуємо користувача
    socket.on('identifyUser', async (user) => {
        
        userName = user;

        await dbAPI.loadAll()
            .then(users => {
                console.log(users);
                users.forEach(element => {
                    allUsers.push(element);
                    if(element.Login == userName)
                    {
                        myUserID = element.id;
                        console.log(myUserID);
                    }
                });
                users.forEach(element => {
                    if(element.id != myUserID)
                    {
                        if(myUserID < element.id)
                        {
                            let room = "room" + myUserID + element.id;
                            socket.join(room);
                        }
                        else
                        {
                            let room = "room" + element.id + myUserID;
                            socket.join(room);
                        }
                    }
                });
                //console.log(allUsers);
                console.log('Sending users');
                socket.emit('users', users);
            })
            .catch(err => {
                console.error('Error loading users:', err);
            });

        /*// повідомлення для того, хто під'єднався
        socket.emit('message', formatMessage('chatBot', // rename formatMessage
            `Welcome to chat ${userName}!`));
    
        // broadcast, для всіх окрім того, хто законектився
        // попереднє лише для того, хто законектився
        socket.broadcast.emit('message', 
            formatMessage('chatBot', `New user(${userName}) has joined the chat!`)); 
            // rename formatMessage

        // а це прям для всіх, і того, хто під'єднався
        // і для всіх інших
        //io.emit();*/



        const userRooms = await myMongoAPI.getAllCollectionOfUser(myUserID, allUsers);
        console.log("Here after");
        //await sendRooms(socket, userRooms);
        userRooms.forEach(async (room) => {
            socket.join(room);
            socket.emit("addNewRoom", room);
        });

        let chatsToRead = await myMongoAPI.getUnreadChat();
        console.log("usimg unread:\n" + chatsToRead);
        chatsToRead.forEach(element => {
            console.log("send:\n" + element);
            socket.emit("UnreadChats", element);
        });
    });

    socket.on('personalRoom', async (roomID, userID) => {
        //socket.leaveAll();
        /*const isAlreaadyInRoom = socket.rooms.has(roomID);
        //console.log("Room is:" + isAlreaadyInRoom);
        if(isAlreaadyInRoom == true)
        {
            return;
        }*/

        socket.join(roomID);
        console.log("Room id:" + roomID + "\tUserID:" + userID);

        dbAPI.loadAll()
            .then(users => {
                let anotherUserName = '';

                users.forEach(element => {
                    if(element.id == userID)
                    {
                        anotherUserName = element.Login;
                    }
                });

                socket.emit('message', 
                    messageAPI.formatMessage('chatBot', 
                        `It is chat between ${userName} and ${anotherUserName}`), roomID);
            })
            .catch(err => {
                console.error('Error loading users:', err);
            });            

        myMongoAPI.createNewCollection(roomID);
        const messages = await myMongoAPI.loadAllMessages(roomID);
        console.log("\n\n\n" + messages.length + "\nIt is server.js\n" + messages);

        for (let messageElement of messages) {

            //console.log("start for");

            const msgLoaded = 
                messageAPI.formatMessageLoaded(
                    messageElement.attributes.sender,
                    messageElement.messageText,
                    messageElement.attributes.time);
            io.to(roomID).emit('message', msgLoaded, roomID);

            //console.log("end for");
        }

        /*messages.forEach(element => {
            const msgLoaded = 
                messageAPI.formatMessage(
                    element.attributes.sender,
                    element.messageText,
                    element.attributes.tieme);
            io.to(roomID).emit('message', msgLoaded);
        });*/
    });

    socket.on('disconnect', () => {
        /*io.emit('message', 
            formatMessage('chatBot', `The user(${userName}) has left the chat!`));*/
            // rename formatMessage
    });

    // ловимо повідомлення
    socket.on('chatMessage', (roomID, msg) => {
        //console.log(msg);
        //для всіх
        //io.emit('message', formatMessage(userName, msg));

        console.log(socket.rooms.has(roomID));
        console.log("Room id:" + roomID + "\tmsg: " + msg);

        let objMsg = messageAPI.formatMessage(userName, msg);
        myMongoAPI.addNewMessage(roomID, objMsg);

        io.to(roomID).emit('message', objMsg, roomID);
    });

    socket.on('groupChat', async (chatName, members) => {     
        let mapNameID = new Map(); 

        let myID;
        let membersID = [];
        allUsers.forEach(element => {
            console.log(element);
            if(element.Login == userName)
            {
                myID = element.id;
                console.log("Myid: " + myID);
            }
            members.forEach(member => {
                if(member == element.Login)
                {
                    //member.id = element.id;
                    mapNameID.set(member, element.id);
                    console.log('member.id: ' + mapNameID.get(member));
                }
            });
        });
        let rooms = [];
        members.forEach(member => {
            if(myID < mapNameID.get(member))
            {
                rooms.push('room' + myID + mapNameID.get(member));
            }
            else 
            {
                rooms.push('room' + mapNameID.get(member) + myID);
            }
        });

        rooms.forEach(room => {
            socket.join(room);
            console.log("Room: " + room);
            socket.broadcast.to(room)
                .emit('addNewRoom', chatName);
        });


        //socket.leaveAll();
        socket.join(chatName);
        console.log("Room: " + chatName + "\nUsers:" + members);



        await myMongoAPI.createNewGroupChatCollection(chatName, members);
        const messages = await myMongoAPI.loadAllMessages(chatName);
        console.log("\n\n\n" + messages.length + "\nIt is server.js\n" + messages);

        for (let messageElement of messages) {

            //console.log("start for");
            console.log("object one: " + messageElement);
            console.log("members" in messageElement);
            console.log(messageElement.hasOwnProperty("members"));
            //if("members" in messageElement)
            console.log(typeof messageElement);
            let m = messageElement['members'];
            console.log(m);
            console.log(messageElement['__v']);
            console.log(JSON.stringify(messageElement));

            let jsonObj = JSON.stringify(messageElement);
            let obj = JSON.parse(jsonObj);
            console.log(obj);

            //if(messageElement.hasOwnProperty("members"))
            if(obj.hasOwnProperty("members"))
            {
                console.log("HERE!");
                continue;
            }
        


            const msgLoaded = 
                messageAPI.formatMessageLoaded(
                    messageElement.attributes.sender,
                    messageElement.messageText,
                    messageElement.attributes.time);
            io.to(chatName).emit('message', msgLoaded, chatName);

            console.log("end for");
        }        
    });

    socket.on('addNewRoom', chatName => {
        socket.join(chatName);
        //console.log(socket.rooms.has(chatName));
        //socket.emit('addNewRoom', chatName);
    });

    socket.on('loadGroupChatMembersNMsgs', async (chatName) => {

        const messages = await myMongoAPI.loadAllMessages(chatName);


        for (let messageElement of messages) {

            //console.log("start for");

            let jsonObj = JSON.stringify(messageElement);
            let obj = JSON.parse(jsonObj);
            console.log(obj);

            if(obj.hasOwnProperty('members'))
            {
                let memberOfChat = messageElement;
                socket.emit('loadMembers', memberOfChat, chatName);    
                continue;
            }


            const msgLoaded = 
                messageAPI.formatMessageLoaded(
                    messageElement.attributes.sender,
                    messageElement.messageText,
                    messageElement.attributes.time);
            io.to(chatName).emit('message', msgLoaded, chatName);

            //console.log("end for");
        }        
    });

    socket.on('leaveRoom', room => {
        console.log("leaving: " + room + " " + userName);
        socket.leave(room);
        io.to(room).emit("message", 
            messageAPI.formatMessage(userName, `The user(${userName}) left room`), room);
        myMongoAPI.deleteFromChat(room, userName);
    });
});

function sendRooms(socket, userRooms) {
    return new Promise((resolve, reject) => {
        for(let room of userRooms)
        {
            socket.join(room);
            socket.emit("addNewRoom", room);
        }  
        resolve(true);
    });    
}


//app.use(express.json());
//app.use(express.bodyParser);
const bodyParser = require('body-parser');
app.use(bodyParser.json());

const urlencodedParser = bodyParser.urlencoded({extended: false});

app.post('/todo', urlencodedParser, async (req, res) => {
    const data = req.body;
    console.log("Data form client", data);
    
    try {
        const result = await myMongoAPI.createORaddToDoList(data.user, 
            data.action, data.text, data.date);
        res.send({result: "The task saved correctly!"});
    } catch (error) {
        res.send({result: "The task didn't save!"});
    }
});

app.post('/todo/change', urlencodedParser, async (req, res) => {
    const data = req.body;
    console.log("Data form client (change)", data);

    try {
        const result = await myMongoAPI.updateList(data.user, 
            data.action, data.text, data.date, data._id);
        console.log(result);
        res.send({result: "The task saved correctly!"});
    } catch (error) {
        console.log(error);
        res.send({result: "The task didn't save!"});
    }
});

app.post('/todo/changeTaskAction', urlencodedParser, async (req, res) => {
    const data = req.body;

    try {
        const result = await myMongoAPI.next(data);
        console.log(res);
        res.send({result: "The task saved correctly!"});
    } catch (error) {
        console.log(error);
        res.send({result: "The task didn't save!"});
    }
});

app.get('/loadAllTasks?', async (req, res) => {
    const user = req.query.user;
    console.log(user)

    try {
        const result = await myMongoAPI.loadAlltasks(user);
        console.log(result);
        res.send( {arrays: result });
    } catch (error) {
        console.log("Error using mongodb functions:", error);
    }

});

const PORT = 3000 || process.env.PORT;

server.listen(PORT, () => console.log(`server port: ${PORT}`));