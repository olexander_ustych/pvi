const messages = require('./messages');
const dbAPI = require('./myDBapi');
const mongoose = dbAPI.mongoose;
const allUsers = require("../server").allUsers;

const messageSchema = new mongoose.Schema({
    messageText : String,
    attributes: Object,
});

const chatMembersSchema = new mongoose.Schema({
    members: Array
});

function trying() {
    const _try_model = mongoose.model('sometry', messageSchema, 'chatExample');
    const _try = new _try_model({
        messageText : "It is try!",
        attributes : {
            sender : "Me",
            time : "Now"
        }
    });

    _try.save()
        .then((result) => {
            console.log("Document saved successfully: ", result);
        })
        .catch((error) => {
            console.error("Error saving doc: ", error);
        });
}

let MessageModel = {};
function createNewCollection(collectionName)
{    
    if(!MessageModel.hasOwnProperty(collectionName))
    {
        MessageModel[collectionName] = mongoose.model(collectionName, messageSchema, collectionName);
    }
    console.log(MessageModel);
}

async function createNewGroupChatCollection(collectionName, members)
{
    console.log(members);
    console.log(!MessageModel.hasOwnProperty(collectionName));
    if(!MessageModel.hasOwnProperty(collectionName))
    {
        MessageModel[collectionName] = mongoose.model('members', chatMembersSchema, collectionName);
        const membersDoc = new MessageModel[collectionName]({
            members: members
        });
        await membersDoc.save()
            .then((result) => {
                console.log("Document saved successfully: ", result);
            })
            .catch((error) => {
                console.error("Error saving doc: ", error);
            });
            
        MessageModel[collectionName] = mongoose.model(collectionName, messageSchema, collectionName);
    }
    console.log(MessageModel);
}

function addNewMessage(collectionName, messageObj) {

    const newItem = new MessageModel[collectionName]({
        messageText : messageObj.text,
        attributes : {
            sender : messageObj.userName,
            time : messageObj.time
        }
    });

    newItem.save()
        .then((result) => {
            console.log("Document saved successfully: ", result);
        })
        .catch((err) => {
            console.error("Error saving doc: ", error);
        });
}
 
async function loadAllMessages(collectionName)
{
    return new Promise( async (resolve, reject) => {     
        if(!MessageModel.hasOwnProperty(collectionName))
        {
            return;
        }
        
        let tempModel = MessageModel[collectionName];
        console.log(tempModel);
        /*tempModel.find({}, (err, messages) => {
            if(err) {
                console.error("Error loading messages from mongoDB: ", err);
                return;
            }
            
            console.log(messages);
            return messages;
        });*/
        
        
        /*const query = await tempModel.find({});
        const messages = await query;
        console.log(messages);
        return messages;*/
        
        
        /*await tempModel.find({}, (err, msgs) => {
            if(err) {
                console.error("Error loading messages from mongoDB: ", err);
                return;
            }
            
            console.log(msgs);
            return msgs;
        });*/
        
        if(mongoose.connection.readyState == 0)
        {
            console.log("0 conn");
            const db = mongoose.connection;

            db.on('error', (err) => {
                console.error('Error connecting to MongoDB:', err);
            });

            db.once('open', () => {
                console.log('Connected to MongoDB');
            });        
        }
        console.log("1 conn");
        
        try {
            const msgs = await tempModel.find({}).exec();
            console.log(msgs);
            console.log("Go out from 'finding'");
            resolve(msgs);
        } catch (err) {
            console.error("Error loading messages from MongoDB:", err);
            reject(err);
        }
    });      
}

let unreadChats = [];
function getAllCollectionOfUser(userID, allUsers) {
    return new Promise(async (resolve, reject) => {

        const db = mongoose.connection;
        //console.log(await db.db.listCollections().toArray());
        const answer = await getCollection(db);
        //db.close();
        console.log(answer);        

        const res = [];
        for (let i = 0; i < answer.length; i++) {            
            if(answer[i] == 'chatExample')
            {
                continue;
            }
            if(answer[i].includes('todo'))
            {
                continue;
            }
            if(answer[i].includes('inProcess'))
            {
                continue;
            }
            if(answer[i].includes('done'))
            {
                continue;
            }
            if(answer[i].includes('room'))
            {
                if(!answer[i].includes(userID.toString()))
                {
                    continue;                
                }
            }
            res.push(answer[i]);
        }
        console.log(res);      

        const rooms = [];
        for (let i = 0; i < res.length; i++) {
            if(res[i].includes('room'))
            {
                rooms.push(res[i]);
                let index = res.indexOf(res[i]);
                res.splice(index, 1);
                i--;
            }            
        }
        console.log(rooms);
        console.log(res);
        

        
        //res.forEach(async (element) => {
        for(let i = 0; i < res.length; i++) 
        {
            if(!MessageModel.hasOwnProperty(res[i]))
            {
                MessageModel[res[i]] = mongoose.model(res[i], messageSchema, res[i]);
                console.log(MessageModel[res[i]]);
            }

            const msgs = await MessageModel[res[i]].find({}).exec();
            console.log("First message:\n" + msgs[0]);

            let jsonObj = JSON.stringify(msgs[0]);
            let obj = JSON.parse(jsonObj);

            let name;
            allUsers.forEach(element => {
                if(element.id == userID)
                {
                    name = element.Login;
                }
            });

            console.log(obj);
            console.log("Name: " + name);
            if(!obj['members'].includes(name))
            {
                console.log("Delete");
                let index = res.indexOf(res[i]);
                console.log(index);
                res.splice(index, 1);
                i--;
            }
        }

        const unread = [];
        for (let i = 0; i < rooms.length; i++) {
            if(!MessageModel.hasOwnProperty(rooms[i]))
            {
                MessageModel[rooms[i]] = mongoose.model(rooms[i], messageSchema, rooms[i]);
                console.log(MessageModel[rooms[i]]);
            }

            const msgs = await MessageModel[rooms[i]].find({}).exec();
            if(msgs != undefined)
            {
                if(msgs[msgs.length - 1] != undefined)
                {
                    console.log("Last message:\n" + msgs[msgs.length - 1]);
        
                    let jsonObj = JSON.stringify(msgs[msgs.length - 1]);
                    let obj = JSON.parse(jsonObj);
    
                    let name;
                    allUsers.forEach(element => {
                        if(element.id == userID)
                        {
                            name = element.Login;
                        }
                    });
    
                    if(obj.attributes.sender != name)
                    {
                        unread.push(rooms[i]);
                    }
                }
            }
        }
        for (let i = 0; i < res.length; i++) {
            if(!MessageModel.hasOwnProperty(res[i]))
            {
                MessageModel[res[i]] = mongoose.model(res[i], messageSchema, res[i]);
                console.log(MessageModel[res[i]]);
            }

            const msgs = await MessageModel[res[i]].find({}).exec();
            if(msgs != undefined)
            {
                if(msgs[msgs.length - 1] != undefined)
                {
                    console.log("Last message:\n" + msgs[msgs.length - 1]);
        
                    let jsonObj = JSON.stringify(msgs[msgs.length - 1]);
                    let obj = JSON.parse(jsonObj);

                    let name;
                    allUsers.forEach(element => {
                        if(element.id == userID)
                        {
                            name = element.Login;
                        }
                    });

                    if(obj.attributes.sender != name)
                    {
                        unread.push(res[i]);
                    }
                }
            }
        }
        
        unreadChats = unread;
        console.log("unread:\n" + unreadChats);
        
        console.log("The chats of user:\n" + res);
        resolve(res);
    });
}

function getCollection(db) {
    return new Promise( async (resolve, reject) => {

        try{
            const collectionNames = await db.db.listCollections().toArray();
            console.log(collectionNames);
            const collections = collectionNames.map((collection) => collection.name);
            console.log(collections);
            resolve(collections);
        } catch(err) {
            reject('Error finding collections:', error);
        }
        

        /*db.once('open', async () => {
            try {
                console.log("I am here");
    
                const collectionNames = await db.db.listCollections().toArray();
                console.log(collectionNames);
                const collections = collectionNames.map((collection) => collection.name);
                console.log(collections);

                resolve(collections);
            } catch (error) {
                reject('Error finding collections:', error);
            }
            
            console.log("Here close");
            db.close();
        });   */
    });
}

function getUnreadChat() {
    return new Promise((resolve, reject) => {
        resolve(unreadChats);
    });
}

async function deleteFromChat(room, userName) {
    try {
        let roomModel = MessageModel[room];
        console.log(roomModel);

        const msgs = await roomModel.find({}).exec();
        //console.log(msgs[0]);
        
        let jsonObj = JSON.stringify(msgs[0]);
        let obj = JSON.parse(jsonObj);        

        console.log(obj);

        let _members = obj['members'];
        let index = _members.indexOf(userName);
        console.log(index);
        _members.splice(index, 1);
        console.log(_members);

        //let id = obj['_id'];
        let id = obj['_id'];
        console.log(id);
        /*const res = await roomModel.updateOne({ _id: id }, 
            { members : _members },
            (err, docs) => {
                if(err) {
                    console.error('Error updating document:', err);
                }
                else
                {
                    console.log('Document updated successfully' + docs);
                }    
            }
        );*/

        tempModel = mongoose.model(room, chatMembersSchema, room);
        console.log(tempModel);
        const res = await makeUpdate(tempModel, id, _members);
        

        //console.log(res);
        //console.log(res.matchedCount);
        //console.log(res.modifiedCount);

        //resolve(msgs);
    } catch (err) {
        console.error("Error loading messages from MongoDB:", err);
        //reject(err);
    }    
}

function makeUpdate(room, id, _members) {
    return new Promise( async (resolve, reject) => {
        //const res = await room.updateOne({ _id: id }, 
        /*const res = await room.findByIdAndUpdate({ _id: id }, 
            { $set: { members : _members }}, (err, docs) => {
                if(err)
                {
                    console.error('Error updating document:', err);
                    reject(err);
                }
                else
                {
                    console.log('Document updated successfully');
                    console.log(docs);
                    resolve(docs);
                }
            });*/
            
            
            //.exec();
            /*.then(docs => {
                console.log('Document updated successfully');
                console.log(docs);
                resolve(docs);
            })
            .catch(err => {
                console.error('Error updating document:', err);
                reject(err);
            });*/
        //resolve(res);

        try {
            const res = await room.findByIdAndUpdate(id,
                { $set: { members : _members } }).exec();
            console.log(res);
            resolve(res);
        }
        catch(err) {
            console.error('Error updating document:', err);
            reject(err);
        }
    });
}


const taskSchema = new mongoose.Schema({
    text : String,
    date: String
});

function createORaddToDoList(userName, listName, text, date) {
    return new Promise((resolve, reject) => {
        const listFullName = userName + "_" + listName;
        const task = new mongoose.model(listFullName, taskSchema, listFullName);
        const saving = new task({
            text: text, 
            date: date
        });
        saving.save()
            .then(res => {
                console.log("Document (new task) saved successfully: ", res);
                resolve(res);
            })
            .catch(error => {
                console.error("Error saving doc: ", error);
                reject(error);
            });
    });
}

function updateList(userName, listName, text, date, id) {
    return new Promise( async (resolve, reject) => {
        const listFullName = userName + "_" + listName;
        const task = new mongoose.model(listFullName, taskSchema, listFullName);

        try {
            const res = await task.findByIdAndUpdate(id, 
                { $set: { text: text, date: date } }).exec();
            console.log(res);
            resolve(res);   
        } catch (error) {
            console.error("Error saving doc: ", error);
            reject(error);
        }        
    });
}

function next(data) {
    return new Promise(async (resolve, reject) => {
        const ex = data.user + "_" + data.ex;
        const next = data.user + "_" + data.next;

        const deleteFrom = new mongoose.model(ex, taskSchema, ex);
        const addTo = new mongoose.model(next, taskSchema, next);

        try {
            const resDelete = await deleteFrom.findByIdAndRemove(data._id);
            console.log("Deletion status: " + resDelete);
            //resolve(resDelete);
        } catch (error) {
            console.log("Error removing", error);
            //reject(error);
        }

        
        const newAdding = new addTo({
            text: data.text,
            date: data.date
        });
        newAdding.save()
        .then(res => {
            console.log("Document (new task) saved successfully: ", res);
            //resolve(res);
        })
        .catch(error => {
            console.error("Error saving doc: ", error);
            //reject(error)
        });

        //resolve(resDelete + res);

    });
}

function loadAlltasks(user) {
    return new Promise( async (resolve, reject) => {
        try {
            let todoCollectionName = user + "_" + "todo";
            const todo = new mongoose.model(todoCollectionName, taskSchema, todoCollectionName);

            const todoTasks = await todo.find({}).exec();
            //console.log(todoTasks); 


            todoCollectionName = user + "_" + "inProcess";
            const inProcess = new mongoose.model(todoCollectionName, taskSchema, todoCollectionName);

            const inProcessTasks = await inProcess.find({}).exec();
            //console.log(inProcessTasks);

            
            todoCollectionName = user + "_" + "done";
            const done = new mongoose.model(todoCollectionName, taskSchema, todoCollectionName);

            const doneTasks = await done.find({}).exec();
            //console.log(doneTasks); 

            const res = [];
            res.push(todoTasks);
            res.push(inProcessTasks);
            res.push(doneTasks);

            //console.log(res);
            resolve(res);
        } catch (error) {
            console.log("Error loading tasks", error);
            reject(error);
        }
    });
}

module.exports = {
    trying: trying,
    createNewCollection: createNewCollection,
    addNewMessage: addNewMessage,
    loadAllMessages: loadAllMessages,
    getAllCollectionOfUser: getAllCollectionOfUser,
    createNewGroupChatCollection: createNewGroupChatCollection,
    getUnreadChat: getUnreadChat,
    deleteFromChat: deleteFromChat,
    createORaddToDoList: createORaddToDoList,
    updateList: updateList,
    next: next,
    loadAlltasks: loadAlltasks
};