const moment = require('moment');

function formatMessage(userName, text) {
    return {
        userName, 
        text, 
        time: moment().format('llll')
    }
}

function formatMessageLoaded(userName, text, time) {
    return {
        userName, 
        text, 
        time
    }
}

module.exports = {
    formatMessage: formatMessage,
    formatMessageLoaded: formatMessageLoaded
};