const mysql = require('mysql');

// Create a connection
const connection = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: '',
  database: 'my_db'
});

function connect() {  
  // Connect to the database
  connection.connect((err) => {
    if (err) {
      console.error('Error connecting to MySQL database:', err);
      return;
    }
    console.log('Connected to MySQL database');
  });
}

function loadAll()
{
  return new Promise( async (resolve, reject) => {
    await connection.query('SELECT * FROM users', (err, results) => {
      if (err) {
        console.error('Error executing query:', err);
        reject();
      }
      //console.log('Query results:', results);
      resolve(results);
    });
  });
}

const mongoose = require('mongoose');
function connectToMongoDB() {
  const url = 'mongodb://127.0.0.1:27017/chatMessages';
  mongoose.connect(url, {useNewUrlParser: true, useUnifiedTopology: true})
      .then(() => {
          console.log('Connected to MongoDB');
      })
      .catch((err) => {
          console.error('Error connection to MongoDB: ', err);
      });
}

module.exports = {
  connect: connect, 
  loadAll: loadAll,
  connectToMongoDB: connectToMongoDB,
  mongoose: mongoose
};