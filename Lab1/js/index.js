let a_span_point = document.querySelector('.notification');
let span_point = document.querySelector('.point');

a_span_point.addEventListener('dblclick', () => {
    span_point.style.display = 'block';
});

a_span_point.addEventListener('mouseover', () => {
    let newDiv = document.querySelector('.newWindow');
    newDiv.style.display = 'block';

    /*let dialog = document.querySelector('.dialog')
    dialog.show();*/
});

a_span_point.addEventListener('mouseout', () => {
    let newDiv = document.querySelector('.newWindow');
    newDiv.style.display = 'none';
});

let profile = document.querySelectorAll('.equalize-imgs'); 
for(let i = 0; i < profile.length; i++)
{
    let newDiv = document.querySelector('.profile');
    newDiv.style.display = 'none';

    if(i == 0) {
        continue;
    }

    profile[i].addEventListener('click', () => {
        if(newDiv.style.display == 'block')        {
            newDiv.style.display = 'none';
        }else if (newDiv.style.display == 'none') {
            newDiv.style.display = 'block';
        }
    });
}

let plusToDialog = document.querySelector('.div-plus > button');
let dialog = document.querySelector('dialog');
plusToDialog.addEventListener('click', () => {
    dialog.show();
});

let btnOutDialog = document.querySelector('dialog > div > div > button');
btnOutDialog.addEventListener('click', () => {
    dialog.close();
});

let btnCancel = document.getElementById('cancel');
btnCancel.addEventListener("click", () => {
    dialog.close();
});

import { student } from "./student.js";

let btnOk = document.querySelector('.btn-in-footer-diag > div > button');
btnOk.addEventListener("click", () => {
    let group = document.getElementById('Group');
    let inputs = document.querySelectorAll('.input-dialog');
    let gender = document.getElementById('Gender');
    let newStudent = new student(group.value, inputs[0].value, inputs[1].value, gender.value, inputs[2].value);

    let table = document.querySelector('main > div > table > tbody');

    table.insertAdjacentHTML('beforeend', `
        <tr>
            <td><input type="checkbox"></td>
            <td><b>${newStudent.group}</b></td>
            <td><b>${newStudent.firstName + " " + newStudent.lastName}</b></td>
            <td><b>${newStudent.gender}</b></td>
            <td><b>${newStudent.birthday}</b></td>
            <td><button class="status"></button></td>
            <td><input type="button" value="&#9997;" class="edit"><input type="button" value="&#215" class="delete"></td>
        </tr>
    `);

    dialog.close();

    group.value = 0;
    for(let inp of inputs)
    {
        inp.value = '';
    }
    gender.value = 0;

    let btnToDelete = document.querySelectorAll('.delete');
    for(let btn of btnToDelete){
        btn.addEventListener('click', () => {
            let dialogWarm = document.querySelector('.warning');
            dialogWarm.btnDelete = btn;
            dialogWarm.show();
    
            //btnToDelete.parentNode.parentNode.remove();
        });
    }

    let btnStatus = document.querySelectorAll('.status');
    for(let btn of btnStatus)
    {   
        btn.style.backgroundColor = 'green';
    }
    for(let btn of btnStatus)
    {
        btn.addEventListener('click', () => {
            if(btn.style.backgroundColor == 'green')
            {
                btn.style.backgroundColor = 'gray';
            }else if(btn.style.backgroundColor == 'gray')
            {
                btn.style.backgroundColor = 'green';
            }
        });
    }
});



let wnDiag = document.querySelector('.warning');
let btnWnOutDialog = document.querySelector('.warning > div > div > button');
btnWnOutDialog.addEventListener('click', () => {
    wnDiag.close();
});

let btnCancel2 = document.getElementById('cancel2');
btnCancel2.addEventListener("click", () => {
    wnDiag.close();
});

let btnWnOk = document.getElementById('wnOK');
btnWnOk.addEventListener('click', () => {
    wnDiag.btnDelete.parentNode.parentNode.remove();
    wnDiag.close();
});