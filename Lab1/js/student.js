export class student {
    group;
    firstName;
    lastName;
    gender;
    birthday;

    constructor(group, firstName, lastName, gender, birthday) {
        this.group = group;
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;

        let correctBD = ''; 
        correctBD += birthday.substring(8, 10);
        correctBD += '.';
        correctBD += birthday.substring(5, 7);
        correctBD += '.';
        correctBD += birthday.substring(0, 4);

        this.birthday = correctBD;
    }
}